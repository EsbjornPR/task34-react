This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## React Task - Survey Puppy

Survey puppy Task using React framework

### Task 34 - React Component Creation
Phase one is completed.

Create a React for the survey puppy API
Create Login, Register and Dashboard Class components (these will be containers)
Create LoginForm and RegisterForm and DashboardMessage components

### Task 35 - Component Events
Phase two is completed.

Add a click event to the register form's register button. This should make an HTTP request to the following API Endpoint: 
https://survey-poodle.herokuapp.com/v1/api/users/register.

The request excepts the following object:

user: {
    username: <your-username-goes-here>,
    password: <your-password-goes-here>
}
The component should use state to display a loading message while the HTTP request is taking place and a message on the component to indicate if it was successful or not. 

### Task 36 - React Router
Phase three is completed.

Take the current project and implement routing

* Install the React Router Dom
* Define be 3 main routes: Login, Register and Dashboard.
* The first screen that must be shown is the Login. 
* The user must be able to navigate from the Login to the Register page
* On successful Registration, the user must be navigated to the Dashboard programatically.
* Create a 404 Not Found page that is displayed when the user types an unknown path in the address bar of the browser
* The 404 can simply display any image. Checkout Freepik.com for some fun images.