import React, { useState } from 'react';
import { registerUser } from '../../api/user.api';

const RegisterForm = props => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState('');

    const onRegisterClicked = async ev => {
        console.log('Event from form: ', ev.target);

        let success = false;
        setIsLoading(true);
        setRegisterError('');

        try {
            const result = await registerUser(username, password);
            console.log('Result from registerUser: ', result);
            success = 'true';
        } catch (error) {
            setRegisterError(error.message);
        } finally {
            console.log('sucess: ', success);
            setIsLoading(false);
            props.click({
                success
            });
        }
    }

    const onUsernameChange = ev => setUsername(ev.target.value.trim());
    const onPasswordChange = ev => setPassword(ev.target.value.trim());

    return (
        <form>
            <div>
                <label>Username</label><br />
                <input type="text" placeholder="Voer u gebruikersnaam in" onChange={onUsernameChange} />
            </div><br />
            <div>
                <label>Password</label><br />
                <input type="password" placeholder="Kies 'n wagwoord" onChange={onPasswordChange} />
            </div><br />
            <div>
                <button onClick={onRegisterClicked} type="button">Register</button>
            </div>
            { isLoading && <div>Registering user...</div> }
            { registerError && <div> {registerError} </div> }
        </form>
    );
}

export default RegisterForm;