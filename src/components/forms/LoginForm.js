import React, { useState } from 'react';
import { loginUser } from '../../api/user.api';

const LoginForm = props => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [loginError, setLoginError] = useState('');

    const onUsernameChange = ev => setUsername(ev.target.value.trim());
    const onPasswordChange = ev => setPassword(ev.target.value.trim());

    const onLoginClicked = async ev => {

        setIsLoading(true);
        setLoginError('');
        let success = false;

        try {
            const result = await loginUser(username, password);
            console.log('Result from loginUser: ', result.status);
            console.log('Resultat:');
            console.log(result.data);
            if (result.status < 300) {
                success = true;
            }
        } catch (error) {
            console.log('Hej från catch');
            setLoginError(error.message);
        } finally {
            setIsLoading(false);
            props.click(success);
        }
    }


    return (
        <form>
            <div>
                <label>Username</label><br />
                <input type="text" placeholder="Enter your username" onChange={onUsernameChange} />
            </div>
            <br />
            <div>
                <label>Password</label><br />
                <input type="password" placeholder="Enter your password" onChange={onPasswordChange} />
            </div><br />
            <div>
                <button type="button" onClick={onLoginClicked}>Log in</button>
            </div>
            {isLoading && <div>Waiting for response...</div>}
            {loginError && <div> {loginError} </div>}
        </form>

    );
}

export default LoginForm;