import React from 'react';
import LoginForm from '../forms/LoginForm';
import { useHistory, Link } from "react-router-dom";

const Login = () => {

    const history = useHistory();
    // const [loginSuccess, setLoginSuccess] = useState(false);

    const handleLoginClicked = (result) => {
        console.log('Result from parent:');
        // setLoginSuccess(result);
        console.log(result);
        // console.log(loginSuccess);
        if (result) {
            history.replace("/dashboard");
        }
    };

    return (
        <React.Fragment>
            <h3>Login to Survey Puppy</h3>
            <LoginForm click={handleLoginClicked} />
            {/* { loginSuccess && <div>Successful login</div> } */}
            <br/>
            <Link to="/register">Register new account</Link>
        </React.Fragment>
    )
};

export default Login;