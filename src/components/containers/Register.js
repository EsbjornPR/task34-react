import React, { useState } from 'react';
import RegisterForm from '../forms/RegisterForm';
import { Link } from 'react-router-dom';

const Register = () => {

    const [ reqResult, setReqResult ] = useState(false);

    const handleRegisterClicked = (result) => {
        console.log('Triggered from Registerform', result);
        setReqResult(result.success);
    };

    return (
        <React.Fragment>
            <h3>Register an account for Survey Puppy</h3>
            <RegisterForm click={ handleRegisterClicked } />
            { reqResult && <div>The user was successfully registered</div> }
            <br/>
            <Link to="/Login">Do you alreay have an account?</Link>
        </React.Fragment>
    )
};

export default Register;