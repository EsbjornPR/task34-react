import React from 'react';
import { Link } from "react-router-dom";

const NotFound = () => {
    return (
        <div>
            <br/>
            <h1>
                Sorry this page was not found.
            </h1><br/>
            <Link to="/login">Please log in</Link><br/><br/>
            <img src="https://image.freepik.com/free-photo/angry-handsome-man-shouting_1368-5167.jpg" alt="What" />
            <br/>
            <a href="https://www.freepik.com/free-photos-vectors/people">People vector created by vectorpocket - www.freepik.com</a>
        </div>
    );
}

export default NotFound;