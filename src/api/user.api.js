const API_REGISTER_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/register';
const API_LOGIN_URL = 'https://survey-poodle.herokuapp.com/v1/api/users/login';
const headers = { 'Content-Type': 'application/json' };

export const registerUser = (username, password) => {
    return fetch(API_REGISTER_URL, {
        method: 'POST',
        headers,
        body: JSON.stringify({
            user: {
                username,
                password
            }
        })
    }).then(r => r.json())
    .then(response => {
        if (response.status >= 400) {
            throw Error(response.error);
        }
        return response;
    })
}

export const loginUser = (username, password) => {
    console.log('Från user', username, password);
    return fetch(API_LOGIN_URL, {
        method: 'POST',
        headers,
        body: JSON.stringify({
            user: {
                username,
                password
            }
        })
    }).then(r => r.json())
    .then(response => {
        if (response.status >= 400) {
            throw Error(response.error);
        }
        return response;
    })
}


/* From RegisterForm.js 
const fetchOptions = {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        user: {
            username,
            password
        }
    })
}; */
